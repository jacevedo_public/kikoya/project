package com.kikoya.controller;

import com.kikoya.dto.UserLogin;
import com.kikoya.feign.client.UserAPI;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/login")
public class LoginController {

    @Autowired
    private UserAPI loadBalancerUserAPI;
    
     @PostMapping("/authenticate")
    public HttpStatus authenticate(@Valid @RequestBody UserLogin userLogin) {
        return  loadBalancerUserAPI.authenticate(userLogin);
    }
}
