package com.kikoya.feign.client;

import com.kikoya.dto.UserLogin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "user-service", fallback = UserHystrixFallback.class)
public interface UserAPI {

    @PostMapping("/users/authenticate")
    public HttpStatus authenticate(@RequestBody UserLogin user);

}
