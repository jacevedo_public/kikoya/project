package com.kikoya.feign.client;

import com.kikoya.dto.UserLogin;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class UserHystrixFallback implements UserAPI {

    @Override
    public HttpStatus authenticate(UserLogin user) {
        return HttpStatus.GATEWAY_TIMEOUT;
    }

}
