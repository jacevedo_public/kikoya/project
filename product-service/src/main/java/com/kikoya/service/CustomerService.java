package com.kikoya.service;

import com.kikoya.model.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private static final AtomicLong counter = new AtomicLong();
    private static List<Product> customerList;

    static {
        customerList = populateProducts();
    }

    private static List<Product> populateProducts() {
        List<Product> customerList = new ArrayList<>();
       
        return customerList;
    }

    public List<Product> findAll() {
        return customerList;
    }
    public Product findById(long id) {
        return customerList.stream().filter(c->c.getId()==id).findFirst().orElse(null);
    }
    public Product create(Product customer){
        customer.setId(counter.incrementAndGet());
        customerList.add(customer);
        return customer;
    }

}
