package com.kikoya.service;

import com.kikoya.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import com.kikoya.repository.ProductRepository;

@Service
public class ProductService {

    @Autowired
    private ProductRepository<Product> userRepository;

    @Transactional
    public Product findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Transactional
    public List<Product> findAll() {
        List<Product> userList = userRepository.findAll();
        return userList;
    }

    @Transactional
    public Product create(Product product){
        return userRepository.save(product);
    }

    @Transactional
    public Product update(Product product){
        if (!userRepository.existsById(product.getId())) {
            return null;
        }
        return userRepository.save(product);
    }

    @Transactional
    public boolean delete(Long id) {
        if (!userRepository.existsById(id)) {
            return false;
        }
        userRepository.deleteById(id);
        return true;
    }

}
