package com.kikoya.repository;

import com.kikoya.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository<P> extends JpaRepository<Product, Long> {

}
