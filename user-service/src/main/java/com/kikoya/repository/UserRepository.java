package com.kikoya.repository;

import com.kikoya.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository<P> extends JpaRepository<User, Long> {

    List<User> findByUsernameAndPassword(String username, String password);

    User findByEmailAndPassword(String username, String password);

    User findByUsername(String username);

    User findByEmail(String email);
}
