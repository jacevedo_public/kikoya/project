package com.kikoya.service;

import com.kikoya.model.User;
import com.kikoya.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository<User> userRepository;

    @Transactional
    public User findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Transactional
    public User findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }

    @Transactional
    public User findByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return user;
    }

    @Transactional
    public Boolean authenticate(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email, password);
        return user!=null;
    }

    @Transactional
    public List<User> findAll() {
        List<User> userList = userRepository.findAll();
        return userList;
    }

    @Transactional
    public User create(User user){
        return userRepository.save(user);
    }

    @Transactional
    public User update(User user){
        if (!userRepository.existsById(user.getId())) {
            return null;
        }
        return userRepository.save(user);
    }

    @Transactional
    public boolean delete(Long id) {
        if (!userRepository.existsById(id)) {
            return false;
        }
        userRepository.deleteById(id);
        return true;
    }

}
