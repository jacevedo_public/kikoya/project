package com.kikoya.service;

import com.kikoya.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private static final AtomicLong counter = new AtomicLong();
    private static List<User> customerList;

    static {
        customerList = populateProducts();
    }

    private static List<User> populateProducts() {
        List<User> customerList = new ArrayList<>();
       
        return customerList;
    }

    public List<User> findAll() {
        return customerList;
    }
    public User findById(long id) {
        return customerList.stream().filter(c->c.getId()==id).findFirst().orElse(null);
    }
    public User create(User customer){
        customer.setId(counter.incrementAndGet());
        customerList.add(customer);
        return customer;
    }

}
